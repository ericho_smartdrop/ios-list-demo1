//
//  Book.swift
//  demo1
//
//  Created by eric ho on 2/5/2018.
//  Copyright © 2018 example. All rights reserved.
//

import Foundation
import UIKit
class Book{
    var image:UIImage?
    var name:String?
    
    init(string:String,uiimage:UIImage) {
        name = string
        image = uiimage
    }
}
