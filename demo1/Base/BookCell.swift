//
//  BookCell.swift
//  demo1
//
//  Created by eric ho on 2/5/2018.
//  Copyright © 2018 example. All rights reserved.
//

import Foundation
import UIKit

class BookCell: UITableViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var bookName: UILabel!
}
