//
//  DetailTableViewController.swift
//  demo1
//
//  Created by eric ho on 2/5/2018.
//  Copyright © 2018 example. All rights reserved.
//

import UIKit

class DetailTableViewController: UIViewController {

    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var name:UILabel!
    @IBOutlet weak var desLabel:UILabel!
    
    var item :Book? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        self.navigationItem.title = "Book Detail";
        
        imageView.image = item?.image!
        name.text = item?.name!
        desLabel.text = "123 description"
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    

}
