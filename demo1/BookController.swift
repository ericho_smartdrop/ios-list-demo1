//
//  ViewController.swift
//  demo1
//
//  Created by eric ho on 2/5/2018.
//  Copyright © 2018 example. All rights reserved.
//

import UIKit

class BookController: UITableViewController {
    var current_select:Book?
    //關聯到UI畫面上的控制項
    
    ////建立資料陣列

    let item :[Book] = [
        Book(string: "At Rest",uiimage: #imageLiteral(resourceName: "b1")),
        Book(string: "star War",uiimage: #imageLiteral(resourceName: "b2")),
        Book(string: "Car War",uiimage: #imageLiteral(resourceName: "b3"))]
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return item.count
    }
    
     
    //填充UITableViewCell中文字標簽的值
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mycell", for: indexPath) as! BookCell
        cell.img.image = item[indexPath.row].image
        cell.bookName.text = item[indexPath.row].name
        
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let str = "line \(indexPath.row) is clicked. Book \(item[indexPath.row].name! ?? "") "
        print(str)
        current_select = item[indexPath.row]
        performSegue(withIdentifier: "first_segue", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "first_segue" {
            let vc = segue.destination as! DetailTableViewController
            vc.item = current_select
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }




}

